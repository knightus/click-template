// @koala-prepend "jquery-1.11.1.min.js"
// @koala-prepend "moment.min.js"
// @koala-prepend "jquery.dataTables.js"
// @koala-prepend "jquery-ui.min.js"
// @koala-prepend "bootstrap.min.js"
// @koala-prepend "chosen.jquery.min.js"
// @koala-prepend "bootstrap-select.js"
// @koala-prepend "daterangepicker.js"
// @koala-prepend "jquery.steps.min.js"
// @koala-prepend "jquery.stepy.js"
// @koala-prepend "icheck.min.js"
// @koala-prepend "fullcalendar.min.js"

// @koala-prepend "jquery.flot.js"
// @koala-prepend "jquery.flot.valuelabels.js"
// @koala-prepend "jquery.flot.stack.js"
// @koala-prepend "jquery.flot.time.js"
// @koala-prepend "jquery.flot.resize.js"
// @koala-prepend "jquery.flot.categories.js"
// @koala-prepend "jquery.flot.selection.js"

$(function() {
	//initialize center widgets
	/* CENTER ELEMENTS IN THE SCREEN */
    jQuery.fn.center = function() {
        this.css("position", "absolute");
        this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
                $(window).scrollTop()) - 30 + "px");
        this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
                $(window).scrollLeft()) + "px");
        return this;
    };
    
	$('.center').center();
	$('window').resize(function(){
		$('.center').center();
	});
	
	$('.chosen-select').chosen({
		width:'100%'
	});
	$('.chosen-select-search-disabled').chosen({
		width:'100%',
		disable_search_threshold: 10
	});
	
	//activate tooltips
	$('[data-toggle=tooltip]').tooltip({
		container:'body'
	});
	
	//reset the form
	$.each($('form'), function() {
		$(this).get(0).reset();
		$(this).find('select.chosen-select').each(function(i, v) {
			$(v).trigger('chosen:updated');
		});
	});

	$("input[type='reset'], button[type='reset']").click(function(e) {
		e.preventDefault();

		var form = $(this).closest('form').get(0);
		form.reset();

		$(form).find('select.chosen-select').each(function(i, v) {
			$(v).trigger('chosen:updated');
		});
	});
	
	/*script for changing the dot color*/
	$('#questionnaire .dropdown-menu li').on('click',function(){
		
	});
	
	//disable error
	$.fn.dataTableExt.sErrMode = 'throw';
	
	//manipulate the dataTable
	$('.data-table').dataTable({
		"bPaginate" : true,
		"bLengthChange" : false,
		"pageLength" : 10,
		"bFilter" : false,
		"bSort" : false,
		"bInfo" : false,
		"bAutoWidth" : false,
	});
	
	//initialize daterange pickers
	$('.daterangepicke').daterangepicker({
		startDate: moment().subtract(7, 'days'),
        endDate: moment(),
        opens: 'left',
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
	});
	
	/*
	 * Patient Volume: Bar chart
	 */
	var patient_volumn_data = {
		data: [
			["August",55],
			["September",75],
			["October",85]
		],
		color: '#3C8DBC'
	};
	
	if($('#patient_volumn_bar_chart').length>0){
		$.plot('#patient_volumn_bar_chart',[patient_volumn_data], {
			grid: {
				borderWidth:1,
				borderColor:'#F3F3F3',
				tickColor:'#F3F3F3'
			},
			series: {
				bars: {
					show: true,
					barWidth: 0.5,
					fill:true,
					fillColor: '#4F81BD',
					
					align: 'center'
				},
				valueLabels: {
					show: true,
					font: '12px Open Sans'
				}
			},
			xaxis: {
				mode: 'categories',
				tickLength: 0
			}
		});
		
		$.plot('#patient_volumn_bar_chart_2',[patient_volumn_data], {
			grid: {
				borderWidth:1,
				borderColor:'#F3F3F3',
				tickColor:'#F3F3F3'
			},
			series: {
				bars: {
					show: true,
					barWidth: 0.5,
					fill:true,
					fillColor: '#4F81BD',
					
					align: 'center'
				},
				valueLabels: {
					show: true,
					font: '12px Open Sans'
				}
			},
			xaxis: {
				mode: 'categories',
				tickLength: 0
			}
		});
	}
});

$(function(){
	/*Forms: */
    $.each($('.form-section-toggle'),function(){
    	var $this = $(this);
    	var $target_section = $('#'+$this.data('section-id'));
    	
    	//hide target section by default
    	$target_section.hide();
    	
    	$this.on('change',function(){
    		$boolean_flag = $this.is(':checked');
    		if($boolean_flag){
    			$target_section.show();
    		} else {
    			$target_section.hide();
    		}
    	});
    });
});



$(function(){
	
	$('.widgets').sortable({
		connectWith: '.widgets',
		handle:'.panel-move',
		placeholder: 'widget-placeholder',
		//containment:'#widgets'
		start: function(e, ui ){
			ui.placeholder.height(ui.helper.outerHeight());
			ui.placeholder.width(ui.helper.innerWidth()-60);
			ui.placeholder.addClass('widget');
			ui.placeholder.css({
				'margin':'20px',
				'margin-top':'0px',
				'float':'left'
			});
			//$a = ui.helper;
		},
	});
	
	$('#formz').stepy({
		legend:false,
		titleClick:true
	});

});

//Todo page
$(function(){
	$(':checkbox').each(function(i,item){ 
        this.checked = item.defaultChecked; 
	});
	
	$('#widget_todo .tabs li').on('click','a',function(){
		$(this).tab('show');
	});
	
	$('#widget_todo .todo-task-status-toggle').iCheck({
		checkboxClass: 'icheckbox_flat-red',
		radioClass: 'iradio_flat-red'
	}).on('ifChecked',function(){
		$(this).closest('.todo-task').addClass('task-done');
	}).on('ifUnchecked',function(){
		$(this).closest('.todo-task').removeClass('task-done');
	});
	
	$('#widget_todo .todo-task.task-done').iCheck('check');
	
	//calendar
	$('#calendar').fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},
		events: [
			{
				title: 'All Day Event',
				start: '2015-01-01',
				color:'red'
			},
			{
				title: 'Long Event',
				start: '2015-01-07',
				end: '2015-01-10'
			},
			{
				id: 999,
				title: 'Repeating Event',
				start: '2015-01-09T16:00:00'
			},
			{
				id: 999,
				title: 'Repeating Event',
				start: '2015-01-16T16:00:00'
			},
			{
				title: 'Conference',
				start: '2015-01-11',
				end: '2015-01-13'
			},
			{
				title: 'Meeting',
				start: '2015-01-12T10:30:00',
				end: '2015-01-12T12:30:00'
			},
			{
				title: 'Lunch',
				start: '2015-01-12T12:00:00'
			},
			{
				title: 'Meeting',
				start: '2015-01-12T14:30:00'
			},
			{
				title: 'Happy Hour',
				start: '2015-01-12T17:30:00'
			},
			{
				title: 'Dinner',
				start: '2015-01-12T20:00:00'
			},
			{
				title: 'Birthday Party',
				start: '2015-01-13T07:00:00'
			},
			{
				title: 'Click for Google',
				url: 'http://google.com/',
				start: '2015-01-28'
			}
		]
	});
});