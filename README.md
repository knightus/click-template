# Click Template#

HTML Template for ClickMedix project in 2014. It was created based on current layout (at that time) without any help from designers. The goal is:

- Improve colors & graphics
- Organize a large amount of data in a better way

## Notes

- This template was intended for layout showcase. No interactive features were implemented.
- The code structure was not so up-to-date. That's how I wrote code in 2014.